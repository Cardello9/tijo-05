public class Point2D {
    private Double x, y;

    Point2D(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return "x: " + this.getX().toString() + " y: " + this.getY().toString();
    }

    public Double getX() {
        return this.x;
    }

    public Double getY() {
        return this.y;
    }
}
