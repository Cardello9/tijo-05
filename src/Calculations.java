class Calculations {
    public static Point2D positionGeometricCenter(Point2D[] point) {
        Double sumX = Double.valueOf(0);
        Double sumY = Double.valueOf(0);
        Integer numEl = 0;

        for(int i=0; i<point.length; i++) {
            sumX += point[i].getX();
            sumY += point[i].getY();
            numEl += 1;
        }
        return new Point2D((sumX/numEl), (sumY/numEl));
    }
    public static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoint) {
        Double sumX = Double.valueOf(0);
        Double sumY = Double.valueOf(0);
        Integer sumMass = 0;

        for(int i=0; i<materialPoint.length; i++) {
            sumX += materialPoint[i].getMass() * materialPoint[i].getX();
            sumY += materialPoint[i].getMass() * materialPoint[i].getY();
            sumMass += materialPoint[i].getMass();
        }
        //return new Point2D((sumX/sumMass), (sumY/sumMass));
        return new MaterialPoint2D((sumX/sumMass), (sumY/sumMass), sumMass);
    }
}