public class MaterialPoint2D extends Point2D{
    private Integer mass;

    MaterialPoint2D(Double x, Double y, Integer mass) {
        super(x, y);
        this.mass = mass;
    }

    public Integer getMass() {
        return this.mass;
    }

    public String toString() {
        return super.toString() + " mass: " + this.getMass().toString();
    }

    public Double getX() {
        return super.getX();
    }

    public Double getY() {
        return super.getY();
    }

}
